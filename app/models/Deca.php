<?php
// app/models/Deca.php


	class Deca extends Eloquent
	{

		//no faria falta però millor...
		protected $table='decas';

        // no ens calen created_at ni updated_at
		public $timestamps=false;


		// funcio propersAniversaris
		// retorna un arrai amb els propers $quants aniversaris de la data del registre actual
      	// iterem des de data de naixement mentre sigui menor que l'actual 
		// podriem mirar dia/mes de data donada amb any actual però si ens donen 29/2 la liem...
		public function propersAniversaris($quants)
			{
			
			$ii=1;
	      	$aDates=array();

			while ($quants>0){
				$eldia=strtotime( $this->data." +$ii years");
				$ii++;
				//si avui-data és negatiu...
				if (time()-$eldia<0) {
					$aDates[]= $eldia;
					$quants--;
					}		
			}
			
			return $aDates;
		}


	}