<?php 

return array(

//mostra.blade.php
'propersXaniversaris'=>'Els teus propers :x aniversaris...',
'holaNom'=>'Hola, :nom.<br>Vares néixer el :neix (era un :diaSetmana).',
'aniversari'=>'El :data serà un :diaSetmana.',
'enviarMail'=>'Enviar email a \':email\'',

//deca.blade.php
'dades'=>'Les teves dades',
'calRevisar'=>'Cal revisar:',
'nom'=>'Nom',
'email'=>'eMail',
'dataNaix'=>'Data naixement',
'teuNom'=>'El teu nom...',
'teuMail'=>'El teu email...',
'elDiaNaix'=>'El dia que vares néixer...',
'enviar'=>'Enviar',

//DecaController.php
'error'=>"Error...",
'temaMail'=>'Els teus aniversaris!',
'mailEnviat'=>'Mail enviat!',

'dataNoValida'=>'La data no és vàlida!',
'calOmplirCamp'=>'Cal omplir el camp ',
'emailNoValid'=>' no és un email vàlid.',


//aniversaris.php (plantilla mail)
'mailSalutacio'=>"Hola :nom!",
'mailSalutacio2'=>"Aquests son els teus propers :quants aniversaris:",
'mailAniversari'=>'El :data serà un :diaSetmana.',


//dies
'dia0' => 'Diumenge',
'dia1' => 'Dilluns',
'dia2' => 'Dimarts',
'dia3' => 'Dimecres',
'dia4' => 'Dijous',
'dia5' => 'Divendres',
'dia6' => 'Dissabte',

);
