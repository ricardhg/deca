<?php 

return array(

//dies
'dia0' => 'Sunday',
'dia1' => 'Monday',
'dia2' => 'Tuesday',
'dia3' => 'Wendesday',
'dia4' => 'Thursday',
'dia5' => 'Friday',
'dia6' => 'Saturday',


//mostra.blade.php
'propersXaniversaris'=>'Your next :x birthdays...',
'holaNom'=>'Hi, :nom.<br>You were born in :neix (it was a :diaSetmana).',
'aniversari'=>'Next :data will be :diaSetmana.',
'enviarMail'=>'Send by email to \':email\'',

//deca.blade.php
'dades'=>'Your data',
'calRevisar'=>'Please check:',
'nom'=>'Name',
'email'=>'eMail',
'dataNaix'=>'Birth date',
'teuNom'=>'Your name...',
'teuMail'=>'Your email...',
'elDiaNaix'=>'Your birthday...',
'enviar'=>'Send',

//DecaController.php
'error'=>"Error...",
'temaMail'=>'Your birthdays!',
'mailEnviat'=>'Mail sent!',

'dataNoValida'=>'Invalid date!',
'calOmplirCamp'=>'Required ',
'emailNoValid'=>' not a valid email address.',


//aniversaris.php (plantilla mail)
'mailSalutacio'=>"Hi :nom!",
'mailSalutacio2'=>"These are your next :quants birthdays:",
'mailAniversari'=>'Next :data will be :diaSetmana.',


);