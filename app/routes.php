<?php

/*
|--------------------------------------------------------------------------
| RUTES
|--------------------------------------------------------------------------
|
*/

Route::get('/', function() { return View::make('deca'); });

Route::post('/',  array('uses' => 'DecaController@desa' ));
Route::get('/{id}',  array('uses' => 'DecaController@mostra' ));
Route::any('/enviamail/{id}',  array('uses' => 'DecaController@enviamail' ));


