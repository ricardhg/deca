<?php



class Lapp {

// rep data en format numeric strtotime i retorna dia de la setmana
// si $maj=false retorna nom en minúscules (a lang inicial és majúscula)
// només verifiquem si E $dia (se suposa q ens arriba una data correcta)
public static function diaSetmana($dia, $maj=false){

    if (!$dia) return '';
    $nomDia=trans('deca.dia'.date('w', $dia));
    return ($maj) ? $nomDia : strtolower($nomDia);

}




// Neteja entrada de scripts, html, style, i gairebé qualsevol cosa perillosa...
public static function neteja($entrada) {

    //return $entrada;

	$cerca = array(
		'@<script[^>]*?>.*?</script>@si',   /* fora javascript */
		'@<[\/\!]*?[^<>]*?>@si',            /* fora HTML tags */
		'@<style[^>]*?>.*?</style>@siU'
	);
	$sortida = preg_replace($cerca, '', $entrada);
	return $sortida;
}



public static function enviamail($msg="Text de prova", $tema="Tema de prova", $destinatari="ricard.herbamel@gmail.com"){

$quienvia = 'prova@prova.com';

$headers = 'From: '.$quienvia . "\r\n" .
    'Reply-To: '. $quienvia . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   

return mail($destinatari, $tema, $msg, $headers);

}


	

}



