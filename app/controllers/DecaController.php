<?php

class DecaController extends \BaseController {


	/**
	 * Mostra els propers aniversaris del deca $id i dóna opció a enviar el mail 
	 */
	public function mostra($id)
	{
		
      	$deca=Deca::find($id);
      	if (!$deca){
      		return trans('deca.error');
      	}

		//obrim mostra i li passem el registre
		return View::make('mostra', ['deca'=>$deca]);

	}
     


	/**
	 * Prepara i envia el mail per al deca $id
	 */
	public function enviamail($id)
	{

      	$deca=Deca::find($id);
      	if (!$deca){
      		return trans('deca.error');
      	}

    	$quants=Input::get('quants',10);
      	$quants=($quants>0) ? $quants : 10;

		$ret = Mail::send('emails.aniversaris', array('deca' => $deca, 'quants'=>$quants), function($message) use ($deca)
		{
		    $message->to($deca->email, $deca->nom)->subject(trans('deca.temaMail'));
		});

		//...
		return trans('deca.mailEnviat');
	}
     


	/**
	 * Mostrem els dies i donem opció a enviar el mail
	 */
	public function desa()
	{
		
       	// validació
		$normes = array(
			'nom' => 'required',
			'email' => 'required|email',
			'data' => 'required|date_format:d/m/Y'
		);
		
		$missatges = array(
			'required' => trans('deca.calOmplirCamp').'\'<strong>:attribute</strong>\'.', 
			'email' => '\''.Input::get('email').'\''.trans('deca.emailNoValid'), 
			'date' => trans('deca.dataNoValida'),
		);
		
		$validacio = Validator::make(Input::all(), $normes, $missatges);

		// process the login
		if ($validacio->fails()) {
			return Redirect::to('/')
				->withErrors($validacio)
				->withInput(Input::all());
		} else {
			// tot ok, guardem a BDD

			$deca = new Deca;
			$deca->nom    = Lapp::neteja(Input::get('nom'));
			$deca->email  = Lapp::neteja(Input::get('email'));
			$eldia= strtotime(strtr(Input::get('data'),'/','-'));
			$deca->data   = date( 'Y-m-d', $eldia);
			$deca->save();
			$id=$deca->id;
                        

            return Redirect::to('/' . $id );
		}
	}



}