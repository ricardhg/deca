@extends('layouts.layout')


@section('cap')

<link rel="stylesheet" href="/css/bootstrap-datepicker3.min.css">
<script type="text/javascript" src="/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datepicker.ca.min.js"></script>

@stop


@section('cos')
	<div class="content">
	<div class="title">{{trans('deca.dades')}}</div>
	<hr>

	@if (Session::has('errors'))
	<div class="errors">
		<h4 class="error">{{trans('deca.calRevisar')}}</h4>
		<ul class="plana">
		@foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>
	<hr>
	@endif

	{{-- FORMULARI --}}

	{{Form::open(array('url'=>'/', 'method'=>'post', 'id'=>'formulari'))}}
	 
	<div class="form-group">
		{{ Form::label('nom', trans('deca.nom')) }}
		{{Form::text(	'nom',
						Input::old('nom'), 
						array(	'type' => 'text',
								'class' => 'form-control', 
								'placeholder'=>trans('deca.teuNom')
								))}}
	</div>


	<div class="form-group">
		{{ Form::label('email', 'Email') }}
		{{ Form::text(	'email',
						Input::old('email'),
						array(	'type' => 'email',
								'class' => 'form-control',
								'placeholder'=>trans('deca.teuMail')
								))}}
	</div>


	<div class="form-group">
		{{ Form::label(	'data', trans('deca.dataNaix')) }}
		{{ Form::text(	'data',
						Input::old('data'),
						array(	'type' => 'text',
								'class' => 'form-control',
								'placeholder' => trans('deca.elDiaNaix'),
								'id' => 'calendari'))}}
	</div>
	<hr>
	{{Form::submit(trans('deca.enviar'), array(	'type' => 'button',
									'class' => 'btn btn-sm btn-success' ))}}
	
	{{Form::close()}}

</div>
@stop


@section('docready')

<script type="text/javascript">
$(document).ready(function() {
    $('#calendari').datepicker({
    	format: "dd/mm/yyyy",
    	endDate: "-1d",
    	language: "ca"
    });
} );
</script>

@stop
