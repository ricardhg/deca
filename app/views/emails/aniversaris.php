<?php

// vista que crea el text del missatge de mail
// rebem $deca i $quants

  	$resposta=array();
	$crlf="<br>";

	$resposta[]=trans('deca.mailSalutacio',['nom'=>$deca->nom]);
	$resposta[]=trans('deca.mailSalutacio2',['quants'=>$quants]);
  	
  	foreach ($deca->propersAniversaris($quants) as $unaData){
  		$resposta[]=
  		trans('deca.mailAniversari',
  			[	'data'=>date('d-m-Y', $unaData),
  				'diaSetmana'=>Lapp::diaSetmana($unaData)]
  			);
  		
  	}

  	echo implode($crlf,$resposta);

