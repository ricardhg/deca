@extends('layouts.layout')


@section('cos')

<?php $quantsAniversaris=10; ?>

<div class="content" id='contingut'>
    <div class="title">{{trans('deca.propersXaniversaris',['x'=>$quantsAniversaris])}}</div>
    <hr>
	<ul class="list-group">
		<li class="list-group-item list-group-item-info">
		{{trans('deca.holaNom',['nom'=>$deca->nom,'neix'=>date('d-m-Y',strtotime($deca->data)), 'diaSetmana'=>Lapp::diaSetmana(strtotime($deca->data))])}}
		</li>
		@foreach ($deca->propersAniversaris($quantsAniversaris) as $unaData)
			<li class="list-group-item">
			{{trans('deca.aniversari',['data'=>date('d-m-Y', $unaData),'diaSetmana'=>Lapp::diaSetmana($unaData)])}}
			</li>
		@endforeach
		<li class="list-group-item list-group-item-danger">
		<a class="btb btn-md btn-alert" type="btn" href="javascript:enviamail({{$deca->id}},{{$quantsAniversaris}})">
		{{trans('deca.enviarMail', ['email'=>$deca->email])}}
		</a>
		</li>
	</ul>
</div>

@stop


@section('docready')

<script>
function enviamail(id,quantsA) {
    $.ajax({
        type: 'POST',
        url: '/enviamail/'+id,
        data: {quants : quantsA }, 
        success: function(r){
            alert(r);
            }
    });
}
</script>

@stop

