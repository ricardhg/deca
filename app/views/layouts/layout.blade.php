<!doctype html>
<html lang="ca">

<head>

<meta charset="UTF-8">
<title>Deca</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="/css/deca.css">

@yield('cap')

</head>

<body>
    <div class="container">
        @yield('cos')
    </div>
</body>

@yield('docready')


</html>
